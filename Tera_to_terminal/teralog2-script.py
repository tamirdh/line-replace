import xlrd
import time
import re


def error_id(error_db):
    xl = xlrd.open_workbook(error_db)
    xs = xl.sheet_by_name("Sheet1")
    errors = {}
    row_lim = xs.nrows
    for row in range(1, row_lim):
        errors[str(int(xs.cell(row, 0).value))] = xs.cell(row, 1).value
    return errors


def log_read(log_file, errors, lsl):
    log = open(log_file, 'r')
    ids = errors.keys()
    last_line = lsl
    logged = log.readlines()
    logged = logged[last_line:]
    new_log = open(log.name.replace(".txt", "") + " new.txt", 'a')
    for line in logged:
        last_line += 1
        if line.replace("\n", "") in ids:
            new_log.write(line.replace(line, errors[line.replace("\n", "")])+"\n")
            print line.replace(line, errors[line.replace("\n", "")])+"\n"
        #elif line[-2] in ids:
         #   new_log.write(line.replace(line[-2], errors[line[-2]]))  # adjust according to protocol
          #  print (line.replace(line[-2], errors[line[-2]]))
        elif len(re.findall('at[+].*', line.lower())) > 0:
            print line.replace(line[0:2], "Working!!!!!")
        else:
            new_log.write(line)
            print line
    new_log.close()
    return last_line


lsl = 0
errors_indicator = error_id("errors.xls") # error DB name addition
while True:
    lsl = log_read("s.txt", errors_indicator, lsl) # log file name addition
    time.sleep(3)

