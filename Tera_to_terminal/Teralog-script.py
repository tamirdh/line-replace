import xlrd
import time


def error_id(error_db):
    xl = xlrd.open_workbook(error_db)
    xs = xl.sheet_by_name("Sheet1")
    errors = {}
    row_lim = xs.nrows
    for row in range(1, row_lim):
        errors[str(int(xs.cell(row, 0).value))] = xs.cell(row, 1).value
    return errors


def log_read(log_file, errors):
    log = open(log_file, 'r')
    ids = errors.keys()
    for line in log.readlines():
        if line.replace('\n', '') in ids:
            print errors[line.replace('\n', "")]

errors_indicator = error_id("errors.xls")
while True:
    log_read("s.txt", errors_indicator)
    print '\n'*5
    time.sleep(3)

