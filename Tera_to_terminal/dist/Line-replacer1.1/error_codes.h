/*
 * error_codes.h
 *
 *  Created on: Jun 21, 2017
 *      Author: TBP
 */

#ifndef ERROR_CODES_H_
#define ERROR_CODES_H_

//TODO: change file name - these are all debug prints!

//TODO: change the version number whenever codes change!!
#define PRINT_CODES_VERSION 0

//TODO:
//1. find a way to remove the duplication of '@' and '\n'.
//2. find a way to add '!' when wanted.
//3. find a way to do this directly with numbers, to remove the strings all together (currently each print still costs us 3 bytes).
//4. unite similar prints and create standartization.

//REPLACEMENT RULES

//1. "_",' '
//2. "__",':'

//END OF REPLACEMENT RULES

//ERROR CODES:

#define MALLOC_ERROR                                  "@3\n"
#define REALLOC_ERROR                                 "@4\n"
#define UNKNOWN_COMMAND                               "@5\n"
#define CRC_MATCH                                     "@6\n"
#define CANT_SEND_TO_SERVER                           "@7\n"
#define WATCHDG_BARKED_AND_RESET_CHIP                 "@8\n"
#define WD_NOT_RUNNING                                "@9\n"
#define WD_RUNNING                                    "@10\n"
#define UART_LAYER_BUFFER_OVERFLOW                    "@11\n"
#define NO_CURRENT_HEADER                             "@12\n"
#define INIT_LOG_FAILED                               "@13\n"
#define INIT_HEADER                                   "@14\n"
#define TEMP_LOG_OVERFLOW                             "@15\n"
#define WROTE_TEMPERATURE_ENTRY                       "@16\n"
#define READ_LOG_ERROR                                "@17\n"
#define READ_HEADER_ERROR                             "@18\n"
#define UPDATE_HEADER_ERROR                           "@19\n"
#define CLEARED_LOG                                   "@20\n"
#define BAD_LEN                                       "@21\n"
#define DONE                                          "@22\n"
#define WARNING_MySlstart_CALLED_BUT_gSlState_IS_ON   "@23\n"
#define TURNED_gSlState_ON                            "@24\n"
#define WARNING_MySlstop_CALLED_BUT_gSlState_IS_OFF   "@25\n"
#define TURNED_gSlState_OFF                           "@26\n"
#define UNDEFINED                                     "@27\n"
#define STILL_UNDER_VOLTAGE__HIBERNATING              "@28\n"
#define UNDER_VOLTAGE__HIBERNATING                    "@29\n"
#define INITIALIZING_STATE_AND_SIM868                 "@30\n"
#define RUNNING_GNSS_SCAN                             "@31\n"
#define GPS_NOT_CONVERGING                            "@32\n"
#define RUNNING_TOWER_SCAN                            "@33\n"
#define RESET_I2C                                     "@34\n"
#define COULD_NOT_ALLOCATE_WLAN_BUF                   "@35\n"
#define UART_WAKEUP_BUT_NO_SMS                        "@36\n"
#define FINAL_TRANSITION                              "@37\n"
#define WLAN_DEBUG_MODE                               "@38\n"
#define SCANNING_WLAN                                 "@39\n"
#define COULD_NOT_CONFIGURE_SL                        "@40\n"
#define COULD_NOT_ESTABLISH_WLAN_CONNECTION           "@41\n"
#define DNS_FAILED                                    "@42\n"
#define TRYING_TO_FLUSH_ALL_LOGS_TO_SERVER            "@43\n"
#define PASSIVE_SCAN                                  "@44\n"
#define RESET                                         "@45\n"
#define WAITING_15_SECONDS                            "@46\n"
#define PROBABLY_NOT_FIRST_SESSION                    "@47\n"
#define ERROR_IN_FIRST_SESSION_ROUTINE                "@48\n"
#define CHOOSING_GPS_UART                             "@49\n"
#define STOP_LOG                                      "@50\n"
#define START_LOG                                     "@51\n"
#define CHOOSING_MODEM_UART                           "@52\n"
#define GNSS_DISABLED                                 "@53\n"
#define NOT_RUNNING_TOWER_SCAN_BECUASE                "@54:\n"
#define RUN_TOWER_SCAN_IS_OFF                         "@55\n"
#define GOT_GPS_FIX                                   "@56\n"
#define PERFORMING_WLAN_SCAN                          "@57\n"
#define NOT_RUNNING_WLAN_SCAN_BECAUSE                 "@58:\n"
#define RUN_WLAN_SCAN_IS_OFF                          "@59\n"
#define WLAN_DEBUG_MOD_ALREADY_AQUIRED_APs            "@60\n"
#define KEY_SWAP                                      "@61\n"
#define LEAVING_FLIGHT_AWARE_MODE                     "@62\n"
#define REOPENING_WLAN_CONNECTION                     "@63\n"
#define ERROR_IN_INDEX                                "@64\n"
#define RESETTING_SIM868                              "@65\n"
#define HIB__ENTERING_HIBERNATE                       "@66\n"
#define HIB__STOPPING_WLAN                            "@67\n"
#define HIB__END                                      "@68\n"
#define WRITE_OCS_REGS                                "@69\n"
#define HIBERNATING                                   "@70\n"
#define CREATED_OR_READ_LOG_COUNTER                   "@71\n"
#define CREATED_LOG                                   "@72\n"
#define FLUSHED_TO_LOG                                "@73\n"
#define ERROR_IN_CLOSING_LOG                          "@74\n"
#define CLOSED_LOG                                    "@75\n"
#define REGS_FILE_DOESNT_EXIST                        "@76\n"
#define ERROR_IN_CREATE                               "@77\n"
#define CREATED                                       "@78\n"
#define REGS_FILE_EXIST                               "@79\n"
#define ERROR_IN_CLOSING                              "@80\n"
#define ERROR_IN_OPENING                              "@81\n"
#define UPDATED                                       "@82\n"
#define BRIEFCA_STATE__                               "@83\n"
#define GNSS_BOOT__COLD                               "@84\n"
#define GNSS_BOOT__WARM                               "@85\n"
#define SYSTEM_BOOT__COLD                             "@86\n"
#define SYSTEM_BOOT__WARM                             "@87\n"
#define NORMAL_                                       "@88\n"
#define RAPID1_                                       "@89\n"
#define RAPID2_                                       "@90\n"
#define RAPID3_                                       "@91\n"
#define NO_WAIT_                                      "@92\n"
#define BRIEFCA_OFF_                                  "@93\n"
#define UNDER_VOLTAGE_                                "@94\n"
#define LOGGER_ONLY_                                  "@95\n"
#define POST_FLIGHT_                                  "@96\n"
#define FLIGHT_AWARE_                                 "@97\n"
#define UNKNOWN_STATE                                 "@98!\n"
#define RUN_GPS__OFF                                  "@99\n"
#define RUN_GPS__ON                                   "@100\n"
#define RUN_TOWER_SCAN__OFF                           "@101\n"
#define RUN_TOWER_SCAN__ON                            "@102\n"
#define RUN_WLAN_SCAN__OFF                            "@103\n"
#define RUN_WLAN_SCAN__ON                             "@104\n"
#define SENT_FINAL_TRANSMISSION                       "@105\n"
#define DIDNT_SEND_FINAL_TRANSMISSION                 "@106\n"
#define ENTER_TECH_MODE                               "@107\n"
#define ENTER__i_TO_INIT_STATE                        "@108\n"
#define ENTER__r_TO_DISABLE_DTR                       "@109\n"
#define ENTER__e_TO_ENABLE_DTR                        "@110\n"
#define ENTER__u_FOR_UART_MODE                        "@111\n"
#define ENTER__p_FOR_STATE_PRINT                      "@112\n"
#define ENTER__o_FOR_PORT_STATUS                      "@113\n"
#define ENTER__c_xx_TO_SET_COUNTER_IN_HEX             "@114\n"
#define ENTER__k_TO_TOGGLE_POWER_KEY                  "@115\n"
#define ENTER__h_TO_HIBERNATE                         "@116\n"
#define ENTER__t_FOR_TCP_OR_UDP_INIT                  "@117\n"
#define ENTER__l_TO_PRINT_ALL_PREVIOUS_LOGS           "@118\n"
#define ENTER__f_TO_PRINT_FPGA_READ_BITS              "@119\n"
#define ENTER__b_TO_SWITCH_BAUD_RATE                  "@120\n"
#define ENTER__B_TO_RETURN_TO_BRIEFCA_MODE            "@121\n"
#define RAND_FAILED                                   "@122\n"
#define GENERATE_KEY_FAILED                           "@123\n"
#define INIT_STATE                                    "@124\n"
#define DISABLING_DTR                                 "@125\n"
#define ENABLING_DTR                                  "@126\n"
#define RETURNING_TO_BRIEFCA_MODE                     "@127\n"
#define TOGGLED_POWER_KEY                             "@128\n"
#define ENTER_ZZZ_TO_ESCAPE                           "@129\n"
#define READ_BITS                                     "@130\n"
#define DOWN_TO_9600                                  "@131\n"
#define UP_TO_115200                                  "@132\n"
#define DISABLED_LIGHT                                "@133\n"
#define READING_LIGHT_INTERRUPT_FAILED                "@134\n"
#define READING_FAILED                                "@135\n"
#define WRITE_FAILED                                  "@136\n"
#define WRITING_ADDRESS_FAILED                        "@137\n"
#define WRONG_CHANNEL                                 "@138\n"
#define UPDATING_CODE                                 "@139\n"
#define CHANGING_WATCHDOG_TO_240_SECONDS              "@140\n"
#define ALLOCATING_MEMORY                             "@141\n"
#define SIM_NOT_RESPONSIVE                            "@142\n"
#define CFUN_ERROR                                    "@143\n"
#define SUFFICIENT_VOLTAGE_FOR_OTA                    "@144\n"
#define CRC_MATCH_WRITING_TO_MEMORY                   "@145\n"
#define NO_MATCH                                      "@146\n"
#define REBOOT                                        "@147\n"
#define IN_BUTTON_HANDLER                             "@148\n"
#define TIMEOUT                                       "@149\n"
#define TURNING_BRIEFCA_ON                            "@150\n"
#define TURNING_BRIEFCA_OFF                           "@151\n"
#define FIRST_SESSION_OF_THIS_BRIEFCA                 "@152\n"
#define FILE_EXIST                                    "@153\n"
#define ERROR_IN_DELETE                               "@154\n"
#define FILE_DELETED                                  "@155\n"
#define CREATING_FILE                                 "@156\n"
#define FILE_CREATED                                  "@157\n"
#define ERROR_IN_WRITE                                "@158\n"
#define FILE_WRITTEN                                  "@159\n"
#define FILE_CLOSED                                   "@160\n"
#define BAD_RECEPTION                                 "@161\n"
#define NOT_REGISTERED___                             "@162\n"
#define CANT_ATTACH                                   "@163\n"
#define ERROR__COULD_NOT_GET_SERVER_IP                "@164\n"
#define ERROR__DIDNT_GET_OWN_IP                       "@165\n"
#define CHECKING_BATTERY_STATUS                       "@166\n"
#define READING_BATTERY_FAILED                        "@167\n"
#define STOPPED_BECAUSE_BUTTON_WAS_PRESSED            "@168\n"
#define WROTE_CHANGING_PART                           "@169\n"
#define SMS_NOT_IN_FORMAT                             "@170\n"
#define FLIGHT_                                       "@171\n"
#define REPORT_                                       "@172\n"
#define RECOVERY_                                     "@173\n"
#define MOVING_TO_STATE_C1                            "@174\n"
#define MOVING_TO_STATE_C2                            "@175\n"
#define MOVING_TO_STATE_C3                            "@176\n"
#define MOVING_TO_STATE_C4                            "@177\n"
#define BACK_TO_NORMAL                                "@178\n"
#define UNKNOWN_ARGUMENT                              "@179\n"
#define NO_SMS                                        "@180\n"
#define CANT_MASK                                     "@181\n"
#define WRITING_1_TO_GNSS_ENABLE                      "@182\n"
#define TOGGLED_POWER_KEY_ONCE                        "@183\n"
#define LOGS_COUNTER_FILE_DOESNT_EXIST__CREATING_IT   "@184\n"
#define UNABLE_TO_CREATE_LOGS_COUNTER_FILE            "@185\n"
#define CREATED_LOGS_COUNTER_FILE                     "@186\n"
#define UNABLE_TO_WRITE_INTO_LOGS                     "@187\n"
#define UNABLE_TO_WRITE_INTO_LOGS_COUNTER             "@188\n"
#define COUNTER_FILE_EXIST__READING_FROM_IT           "@189\n"
#define UNABLE_TO_READ_FROM_LOGS_COUNTER              "@190\n"
#define UNABLE_TO_CLOSE_LOGS_COUNTER_FILE             "@191\n"
#define UNABLE_TO_OPEN_LOGS_COUNTER_FILE_FOR_WRITE    "@192\n"
#define REACHED_MAX_LOGS__STARTING_FROM_0_AGAIN       "@193\n"
#define END_OF_LOG_CONTENT                            "@194\n"
#define LOGS_COUNTER_FILE_DOESNT_EXIST                "@195\n"
#define UPDATDE_cENC_FILE__                           "@196\n"
#define ERROR_IN_READ                                 "@197\n"
#define tCOUNTER_FILE__                               "@198\n"
#define WROTE_NEW_PART                                "@199\n"
#define AUTH_WAS_ALREADY_AWAKE                        "@200\n"
#define SENDING_NONCE_FAILED                          "@201\n"
#define SENDING_MAC_FAILED                            "@202\n"
#define WRONG_ZONE                                    "@203\n"
#define ERROR__BIG_CHUNK_IN_OTP                       "@204\n"
#define BAD_ANSWER                                    "@205\n"
#define WRONG_SLOT                                    "@206\n"
#define ENCRYPTED                                     "@207\n"
#define NO_CONNECTION                                 "@208\n"
#define COULD_NOT_PARSE_COMMAND                       "@209\n"
#define BAD_BATTERY                                   "@210\n"
#define CONVERSION_ERROR                              "@211\n"
#define ERROR__NAN                                    "@212\n"
#define PARSE_ERROR                                   "@213\n"
#define FAILED_TO_START_THE_DEVICE                    "@214\n"
#define PARSE_TOWERS_FAILED                           "@215!\n"
#define DEVICE_STARTED_AS_STATION                     "@216\n"
#define UNABLE_TO_CLEAR_THE_CONNECTION_POLICY         "@217\n"
#define UNABLE_TO_SET_THE_SCAN_POLICY                 "@218\n"
#define UNABLE_TO_RETREIVE_THE_NETWORK_LIST           "@219\n"
#define UNABLE_TO_CLEAR_THE_SCAN_POLICY               "@220\n"
#define SUCCESS_                                      "@221\n"
#define UART_MODE_ENTER_ZZZ_TO_ESCAPE                 "@222\n"

//END OF ERROR CODES


#endif /* ERROR_CODES_H_ */
