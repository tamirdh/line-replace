"""
This is the local rnu script. A server script will be written as well
This scripts receives a log file (Static or live) and preforms the following:
 1. Gets updated error dictionary from xls file
 2. Reads log file for the first time
 3.Changes the "Error ID" with the relevant error info
 4. Saves the new log file in the format of "Name new.txt"
 5. Prints the new log file
 6. Check if log changes and repeat if necessary
"""
import xlrd
import time
import re
import os


def error_dict_update(xls_file):
    xb = xlrd.open_workbook(xls_file)
    xs = xb.sheet_by_index(0)
    row_lim = xs.nrows
    error_dict = {}

    for row in xrange(1, row_lim):
        Id = xs.cell(row, 0).value
        error = xs.cell(row, 1).value
        error_dict[Id] = error
    return error_dict


def log_read(txt_file):
    with open(txt_file, 'r') as log:
        return log.readlines()


def line_replacer(lines, errors, new_log_name):
    keys = errors.keys()
    for line in lines:
        modify = 0
        for key in keys:
            check = re.findall('%s .*' % key, line)
            if len(check) == 1:
                modify = 1
                modified = line.replace('%s' % check[0], errors[key])
                new_log_name.write(modified)
                print modified
                break
            elif len(check) == 1 and key == "Waiting 15 seconds":
                new_log_name.write(line)
                print line
                modify = 1
                time.sleep(16)
            elif len(check) == 1 and key == "at+cnetscan":
                new_log_name.write(line)
                print line
                modify = 1
                time.sleep(60)
            else:
                continue
        if modify == 0:
            new_log_name.write(line)
            print line


def new_log(name, xls_file, lastline=0):
    new_name = name.replace('.txt', "")+" new.txt"
    log = open(new_name, 'a')
    lines = log_read(name)
    ls = len(lines)
    lines = lines[lastline::]
    errors_dict = error_dict_update(xls_file)
    line_replacer(lines=lines, errors=errors_dict, new_log_name=log)
    log.close()
    return ls


log_file_name = raw_input("Log name: ")
last_line = 0
while True:
    full_name = os.path.join(os.path.expanduser('~/Desktop/Logs-teraterm'), log_file_name + ".txt")  # old log name
    last_line = new_log(full_name, "errors.xls", lastline=last_line)
    break










