import time
import re
import os
import string


class Desktop_log(object):
    last_line = 0                                           # line indicator for loop reading
    lines = []

    def __init__(self, error_DB, log_file, timeout):
        self.errordb = self.errors(error_DB)                # {id: error} type=dict
        self.oldlog = log_file                              # name in format of directory\file.log type=str
        self.newlog = log_file+" new.log"                   # name in format of directory\file.log type=str
        self.timeout = timeout
        self.combiner()

    def reporter(self):
        raw_input("Session ended on log %s \n Press Enter to exit" % self.newlog)

    @staticmethod
    def errors(error_db):
        rules_flag = False
        db = {'rules': {}}
        error_lines = open(error_db, 'r').readlines()
        for line in error_lines:
            if line.find('END OF REPLACEMENT RULES') > 0:
                rules_flag = False
                continue
            elif line.find("REPLACEMENT RULES") > 0:
                rules_flag = True
                continue
            if not rules_flag:
                check = re.findall("#define.*@[0-9]*[:!]*", line)
                if len(check) > 0:
                    ID = re.findall("@[0-9]*", check[0])[0].replace('\\n"', '')
                    error = re.findall('#define.*"?', check[0])[0].replace("#define ", "")
                    error = error.replace(re.findall('"@[0-9]*', error)[0], "")
                    if ID in db.keys():
                        raise ValueError(raw_input("Two errors with the same ID- %s \n Press Enter to exit" % ID))
                    db[ID] = error.replace(" ", "")
            elif rules_flag:
                rule_id = re.findall('".*"', line)
                if len(rule_id) > 0:
                    rule_id = rule_id[0].replace('"', "")
                    rule_replacement = re.findall(",(.*)", line)[0].replace("'", "")
                    db['rules'][rule_id] = rule_replacement
        return db

    def reader(self):
        log = open(self.oldlog, 'r')
        time.sleep(1)
        log_lines = log.readlines()
        modify = log_lines[self.last_line:]
        self.last_line += len(modify)  # index of next line
        if len(modify) > 0:
            valid = modify[-1]
            if valid.find("\n") > 0:
                self.lines += modify
            else:
                modify.pop(-1)
                self.last_line -= 1
                self.lines += modify
        return self.lines

    def parser(self):   # opens new log and writes modified data to it
        n_log = open(self.newlog, 'a')
        self.lines = []
        before_makeover = []
        lines = self.reader()
        for line in lines:
            keys = self.errordb.keys()
            for key in keys:
                check = len(re.findall("%s\n" % key, line))
                if check >= 1:
                    replacement = self.errordb[key]
                    before = line.replace("%s" % key, replacement)
                    before_makeover.append(before)
                    break
                elif key != keys[-1]:
                    continue
                elif key == keys[-1]:
                    before_makeover.append(line)
        for line2 in before_makeover:
            no_letters = ""
            flag = re.findall(".*[_!@#$%^&*]+", line2)
            if len(flag) == 0:
                n_log.write(line2)
                print line2
                continue
            for letter in line2:
                if letter in string.letters + "\n" + " " + string.digits:
                    if len(no_letters) > 0:
                        if no_letters in self.errordb['rules'].keys():
                            line2 = line2.replace(no_letters, self.errordb['rules'][no_letters], 1)
                            no_letters = ""
                            continue
                        else:
                            no_letters = ''
                    continue
                else:
                    no_letters += letter
                    continue
            n_log.write(line2)
            print(line2)
        n_log.close()

    def combiner(self):
        counter_limit = int(self.timeout/0.1)
        counter = 0
        while True:
            if counter == 0:
                self.parser()
            size_check = os.path.getsize(self.oldlog)
            time.sleep(0.1)
            size_check2 = os.path.getsize(self.oldlog)
            if size_check < size_check2:
                self.parser()
                counter = 1
                continue
            elif size_check2 == size_check and counter <= counter_limit:
                time.sleep(0.1)
                size_valid = os.path.getsize(self.oldlog)
                if size_valid > size_check2:
                    self.parser()
                    counter = 1
                    continue
                counter += 1
                continue
            elif counter >= counter_limit:
                break
        self.reporter()


if __name__ == '__main__':
    print "This script is the Desktop version of the line replacer. Please place the logs under the following " \
          "directory- Desktop\Logs-teraterm\n" \
          "Please keep error_codes.h file in the same directory as the script\n"
    print "Version 1.1\n"
    errordb = "error_codes.h"
    name = time.strftime("%Y-%m-%d--%H-%M")
    full_name = os.path.join(os.path.expanduser("~\\Desktop\\Logs-teraterm"), name)
    time_out = int(raw_input("Please enter this session time out (seconds)"))
    try:
        with open(full_name, 'r') as a:
            a.close()
        script = Desktop_log(error_DB=errordb, log_file=full_name, timeout=time_out)
    except IOError:
        name = raw_input("Failed to get name \n Log name: ")
        full_name = os.path.join(os.path.expanduser("~\\Desktop\\Logs-teraterm"), name)
        script = Desktop_log(error_DB=errordb, log_file=full_name, timeout=time_out)
