This documents the line replacer script.
Last edited: 22/06/17
Author: Tamir David- Hay

The script has 2 versions- Desktop and Server.

Desktop version

Requirments:
1. Teraterm auto logging with the following name: %Y-%m-%d--%H-%M (edit under setup -> additional setting -> log) and save (setup->save setup) and make sure setip directory is cinfugured to the correct setup file
2. Logs should be saved to Desktop/Logs-teraterm folder
3. error_codes.h file in the same directory as the script
4. Open the scripts .exe file when opening Tera term

Description:
1. The script opens the logs directory and searches for the log file by name matching (Matches strftime pattern %Y-%m-%d %H-%M)
2. If file not found it will ask for name to be written manually
3. The scripts creates a dictionary from the error_codes.h file in the syntax of ID:Error
4. If it encounters an ID that is already in the dictionary it will raise a value error and terminate (teraterm will still run and the log will still be written)
5. The script reads from the old log and writess the parsed lines (ID -> Error, otherwise does nothing) to a  list 
6. Loops over list and applies rules (a dictionary in the error dictionary)
7. The scripts checks the old log size waits X secs and checks again (Currently X=0.25 seconds)
8. If the size has changed the script will read and parse the log from last line he read and write the parsed lines to the new log 
9. Once the sized hasn't changed for Y amount of time the script will print what log he worked on and terminate (Y is currently 51 seconds)

Server version ---- NOT COMPLETE YET 25/6/17

Requirments:
1. A directory named "decoded" under logging/Log in the logs server
2. The script in the logging directory
3. error-codes.h file in the logging directory

Description:
1. The script read all file names in Log directory ad checks if there are parsed versions of the files in the decoded directory
2. The scripts runs in a loop parsing each log file that does not have a parsed version and creates a parsed version (name+"new")
3. If a raise value error is encountered than an Email will be sent (Currently sending to tamir.dh@brieftrace.com)
4. The script creates a list of file names that have been parsed and sends an email once a day (17:00) with daily parsed logs

