import smtplib
import time
import re
import os
from email.mime.text import MIMEText


class Server_log(object):
    last_line = 0  # line indicator for loop reading
    report = False  # Indicator for log error changes

    def __init__(self, error_DB):
        self.errordb = self.errors(error_DB)  # {id: error} type=dict
        self.oldlog = ''
        self.newlog = ''
        self.name_get()

    @staticmethod
    def name_get():  # get list of files in the current directory
        files = os.listdir((os.getcwd() + "/Log"))
        return files

    def name_check(self):
        un_parsed = []
        for file_name in self.name_get():
            if os.getcwd() + "Log/encoded/%s" % file_name + "-new.log" in self.name_get():
                continue
            elif file_name.find(" new.log") > 0:
                continue
            else:
                un_parsed.append(os.getcwd() + "/Log/%s" % file_name)
        return un_parsed

    @staticmethod
    def reporter(msg_str, flag=False):
        report_time = time.strftime("%Y-%m-%d")
        current_time = time.strftime("%H")
        msg = MIMEText(msg_str)
        if flag:
            msg['Subject'] = "Error in line replacer script"
            msg['From'] = 'Log server'
            msg['To'] = 'tamir.dh@brieftrace.com'
            send = smtplib.SMTP('localhost')
            send.sendmail('tamir.dh@brieftrace.com', msg['To'], msg.as_string())
            send.quit()
        else:
            if current_time == "17":
                msg['Subject'] = "Daily report %s" % report_time
                send = smtplib.SMTP('localhost')
                send.sendmail('tamir.dh@brieftrace.com', 'tamir.dh@brieftrace.com', msg.as_string())
                sent = True
                return sent

    def errors(self, error_db):
        db = {}
        error_lines = open(error_db, 'r').readlines()
        for line in error_lines:
            check = re.findall("#define.*@.*", line)
            if len(check) > 0:
                ID = re.findall("@.*", check[0])[0].replace('\\n"', '')
                error = re.findall('#define.*"?', check[0])[0].replace("#define ", "")
                error = error.replace(re.findall('".*', error)[0], "")
                if ID in db.keys():
                    self.reporter("Two errors with the same ID- %s" % ID, flag=True)
                    raise ValueError("Two errors with the same ID- %s" % ID)
                db[ID] = error.replace(" ", "")
        return db

    def reader(self):
        log = open(self.oldlog, 'r')
        modify = log.readlines()
        modify2 = modify[self.last_line::]
        self.last_line += len(modify2)
        log.close()
        return modify2

    def parser(self):  # opens new log and writes modified data to it
        n_log = open(self.newlog, 'a')
        lines = self.reader()
        for line in lines:
            keys = self.errordb.keys()
            for key in keys:
                check = len(re.findall(".*%s" % key, line))
                if check >= 1:
                    self.report = True
                    replacement = self.errordb[key]
                    line.replace("%s" % key, replacement)
                    n_log.write(line)
                    break
                elif key != keys[-1]:
                    continue
                elif key == keys[-1]:
                    n_log.write(line)
        n_log.close()

    def combiner(self):
        logs_decoded = []
        for log in self.name_check():
            self.oldlog = log  # name in format of directory\file.log type=str
            self.newlog = log.replace("Log/", "Log/decoded/") + "-new.log"  # name in format of directory\file.log
            # type=str

            counter = 0
            while True:
                if counter == 0:
                    self.parser()

                size_check = os.path.getsize(self.oldlog)
                time.sleep(0.5)
                size_check2 = os.path.getsize(self.oldlog)
                if size_check < size_check2:
                    self.parser()
                    counter = 1
                    continue
                elif size_check2 == size_check and counter <= 101:
                    time.sleep(0.5)
                    size_valid = os.path.getsize(self.oldlog)
                    if size_valid > size_check2:
                        self.parser()
                        counter = 1
                        continue
                    counter += 1
                    continue
                elif counter >= 102:
                    break
            logs_decoded.append(self.newlog)
        self.reporter(logs_decoded)


def script():
    while True:
        start = Server_log('error_codes.h')
        start.combiner()
        time.sleep(300)
        continue


script()
