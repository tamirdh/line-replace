import smtplib
import time
import re
import os
from email.mime.text import MIMEText
import string
import datetime
import sys


class Server_log(object):
    last_line = 0  # line indicator for loop reading
    lines = []

    def __init__(self, error_DB):
        self.errordb = self.errors(error_DB)  # {id: error} type=dict
        self.oldlog = ''
        self.newlog = ''

    @staticmethod
    def logging(data):
        log_file = open("Line_replacer_log.log", 'a')
        log_file.writelines(data)
        log_file.close()

    @staticmethod
    def name_get():  # get list of files in the current directory
        files = (filer for filer in os.listdir((os.getcwd() + "/Log")) if
                 os.path.isfile(os.path.join(os.getcwd() + "/Log", filer)))
        return files

    def name_check(self):
        un_parsed = []
        parsed = os.listdir(os.getcwd() + "/Log/decoded/")
        for file_name in self.name_get():
            if file_name + "-new.log" in parsed:
                continue
            elif file_name.find(" new.log") > 0:
                continue
            else:
                file_name = (os.getcwd() + "/Log/%s" % file_name)
                mod_time = os.path.getmtime(file_name)
                nrml_mod_time = datetime.date.fromtimestamp(mod_time)
                now = datetime.datetime.now().date()
                difference = (now - nrml_mod_time).days
                if difference <= 2:
                    un_parsed.append(file_name)
        self.logging(["logs to be parsed this session: %d" % len(un_parsed)])
        return un_parsed

    @staticmethod
    def reporter(msg_str, flag=False):
        report_time = time.strftime("%Y-%m-%d")
        current_time = time.strftime("%H")
        msg = MIMEText(msg_str)
        if flag:
            msg['Subject'] = "Error in line replacer script"
            msg['From'] = 'Log server'
            msg['To'] = 'tamir.dh@brieftrace.com'
            send = smtplib.SMTP('localhost')
            send.sendmail('tamir.dh@brieftrace.com', msg['To'], msg.as_string())
            send.quit()
        else:
            if current_time == "17":
                msg['Subject'] = "Daily report %s" % report_time
                send = smtplib.SMTP('localhost')
                send.sendmail('tamir.dh@brieftrace.com', 'tamir.dh@brieftrace.com', msg.as_string())
                sent = True
                return sent

    def errors(self, error_db):
        rules_flag = False
        db = {'rules': {}}
        error_lines = open(error_db, 'r').readlines()
        for line in error_lines:
            if line.find('END OF REPLACEMENT RULES') > 0:
                rules_flag = False
                continue
            elif line.find("REPLACEMENT RULES") > 0:
                rules_flag = True
                continue
            if not rules_flag:
                check = re.findall("#define.*@[0-9]*[:!]*", line)
                if len(check) > 0:
                    ID = re.findall("@[0-9]*", check[0])[0].replace('\\n"', '')
                    error = re.findall('#define.*"?', check[0])[0].replace("#define ", "")
                    error = error.replace(re.findall('"@[0-9]*', error)[0], "")
                    if ID in db.keys():
                        self.reporter("Two errors with the same ID- %s" % ID, flag=True)
                        raise ValueError("Two errors with the same ID- %s" % ID)
                    db[ID] = error.replace(" ", "")
            elif rules_flag:
                rule_id = re.findall('".*"', line)
                if len(rule_id) > 0:
                    rule_id = rule_id[0].replace('"', "")
                    rule_replacement = re.findall(",(.*)", line)[0].replace("'", "")
                    db['rules'][rule_id] = rule_replacement
        return db

    def reader(self, state):
        log = open(self.oldlog, 'r')
        log_lines = log.readlines()
        modify = log_lines[self.last_line:]
        self.last_line += len(modify)  # index of next line
        if len(modify) > 0:
            valid = modify[-1]
            if valid.find("\n") > 0:
                self.lines += modify
            else:
                if state:
                    self.lines += modify
                else:
                    modify.pop(-1)
                    self.last_line -= 1
                    self.lines += modify
        return self.lines

    def parser(self, state):  # opens new log and writes modified data to it
        n_log = open(self.newlog, 'a')
        self.logging(["started log %s\n" % self.newlog])
        before_makeover = []
        self.lines = []
        lines = self.reader(state)
        self.logging(lines)
        for line in lines:
            keys = self.errordb.keys()
            for key in keys:
                check = len(re.findall("%s\n" % key, line))
                if check >= 1:
                    replacement = self.errordb[key]
                    before = line.replace("%s" % key, replacement)
                    before_makeover.append(before)
                    break
                elif key != keys[-1]:
                    continue
                elif key == keys[-1]:
                    before_makeover.append(line)
        for line2 in before_makeover:
            no_letters = ""
            flag = re.findall(".*[_!@#$%^&*]+", line2)
            if len(flag) == 0:
                n_log.write(line2)
                print line2
                continue
            for letter in line2:
                if letter in string.letters + "\n" + " " + string.digits:
                    if len(no_letters) > 0:
                        if no_letters in self.errordb['rules'].keys():
                            line2 = line2.replace(no_letters, self.errordb['rules'][no_letters], 1)
                            no_letters = ""
                            continue
                        else:
                            no_letters = ''
                    continue
                else:
                    no_letters += letter
                    continue
            n_log.write(line2)
            print(line2)
        n_log.close()

    def combiner(self, param=''):
        if param != '':
            self.oldlog = param
            self.newlog = self.newlog = param.replace("Log/", "Log/decoded/") + "-new.log"
            self.parser(True)
        elif param == '':
            files = self.name_check()
            for log in files:
                self.oldlog = log  # name in format of directory\file.log type=str
                self.newlog = log.replace("Log/", "Log/decoded/") + "-new.log"  # name in format of directory\file.log
                # type=str

                counter = 0
                while True:
                    if counter == 0:
                        self.parser(False)

                    size_check = os.path.getsize(self.oldlog)
                    time.sleep(0.5)
                    size_check2 = os.path.getsize(self.oldlog)
                    if size_check < size_check2:
                        self.parser(False)
                        counter = 1
                        continue
                    elif size_check2 == size_check and counter <= 101:
                        time.sleep(0.5)
                        size_valid = os.path.getsize(self.oldlog)
                        if size_valid > size_check2:
                            self.parser(False)
                            counter = 1
                            continue
                        counter += 1
                        continue
                    elif counter >= 102:
                        self.parser(True)
                        old_lines_opening = open(self.oldlog, 'r')
                        old_lines = len(old_lines_opening.readlines())
                        old_lines_opening.close()
                        new_lines_opening = open(self.newlog, 'r')
                        new_lines = len(new_lines_opening.readlines())
                        self.logging(["Old length: %s\n" % old_lines, "New length: %s\n" % new_lines])
                        break


def script(param=''):
    if param == '-H' or param == '--help':
        print "This script is the server version of the line replacer script \n It runs itself on the server \n If " \
              "you want to run it on a specific log use the following syntax:\n -H or --help will display this " \
              "message \n" \
              "-N= or --name= will run it on the requested log\n"
    elif param == ('-N' or '--name'):
        param = re.findall("=(.*)")[0]
        start = Server_log('error_codes.h')
        start.combiner(param)
    else:
        while True:
            start = Server_log('error_codes.h')
            start.combiner('')
            time.sleep(30)
            print "SESSION FINISHED"
            continue


trigger = sys.argv
if len(trigger) >= 2:
    param = trigger[1]
    script(param)
else:
    script()
