# -*- mode: python -*-

block_cipher = None


a = Analysis(['Desktop-V1.0.py'],
             pathex=['C:\\Users\\Tamir David Hay\\Anaconda2\\Lib\\site-packages', 'C:\\Users\\Tamir David Hay\\PycharmProjects\\line-replacement\\Tera_to_terminal'],
             binaries=[],
             datas=[],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='Desktop-V1.0',
          debug=False,
          strip=False,
          upx=True,
          console=True )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               name='Desktop-V1.0')
