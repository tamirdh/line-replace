"""
Notes for V0.6- add a better report system that can be on server with no user interference
"""

import xlrd
import time
import re
import os


class Desktop_log(object):

    last_line = 0                                           # line indicator for loop reading
    report = False                                          # Indicator for log error changes

    def __init__(self, error_DB, log_file):
        self.errordb = self.errors(error_DB)                # {id: error} type=dict
        self.oldlog = log_file                              # name in format of directory\file.log type=str
        self.newlog = log_file+" new.log"                   # name in format of directory\file.log type=str
        self.combiner()

    def reporter(self):
        if self.report:
            print "Check log : %s" %self.newlog

    def errors(self, error_db):
        db = {}
        xb = xlrd.open_workbook(error_db)
        xs = xb.sheet_by_index(0)
        rowlim = xs.nrows
        for row in xrange(1, rowlim):
            error = xs.cell(row, 1).value
            ID = xs.cell(row, 0).value
            db[ID] = error
            continue
        return db

    def reader(self):
        log = open(self.oldlog, 'r')
        modify = log.readlines()
        modify2 = modify[self.last_line::]
        self.last_line += len(modify2)
        log.close()
        return modify2

    def parser(self):   # opens new log and writes modified data to it
        n_log = open(self.newlog, 'a')
        lines = self.reader()
        for line in lines:
            keys = self.errordb.keys()
            for key in keys:
                check = len(re.findall(".*%s" % key, line))
                if check >= 1:
                    self.report = True
                    replacement = self.errordb[key]
                    line.replace("%s" % key, replacement)
                    n_log.write(line)
                    break
                elif key != keys[-1]:
                    continue
                elif key == keys[-1]:
                    n_log.write(line)
        n_log.close()

    def combiner(self):
        counter = 0
        while True:
            if counter == 0:
                self.parser()

            size_check = os.path.getsize(self.oldlog)
            time.sleep(0.5)
            size_check2 = os.path.getsize(self.oldlog)
            if size_check < size_check2:
                self.parser()
                counter = 1
                continue
            elif size_check2 == size_check and counter <= 101:
                time.sleep(0.5)
                size_valid = os.path.getsize(self.oldlog)
                if size_valid > size_check2:
                    self.parser()
                    counter = 1
                    continue
                counter += 1
                continue
            elif counter >= 102:
                break
        self.reporter()


if __name__ == '__main__':
    errordb = "errors.xls"
    name = raw_input("Log name: ")
    full_name = os.path.join(os.path.expanduser("~\\Desktop\\Logs-teraterm"), name)
    script = Desktop_log(error_DB=errordb, log_file=full_name)




