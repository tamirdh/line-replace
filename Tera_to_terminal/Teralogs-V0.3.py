import xlrd
import time
import re


def error_id(error_db):
    """
    :param error_db: xls file written by protocol
    :return: dict {id:error}
    """
    xl = xlrd.open_workbook(error_db)
    xs = xl.sheet_by_name("Sheet1")
    errors = {}
    row_lim = xs.nrows
    for row in range(1, row_lim):
        errors[str(int(xs.cell(row, 0).value))] = xs.cell(row, 1).value
    return errors


def log_read(log_file, errors, lsll = 0):
    """
    :param log_file: tera term log file (txt)
    :param errors: xls file written by protocol
    :param lsll: last line read from. default = 0
    :return: last line read and len for stopping the script if needed
    this func calls error_id to retrive the dict
    open tera term log file and reads from it
    creates a new log file and writes to it the modified data (still needs a protocol for what modifications should
    happen)
    returns leaving point of the readlines() list and that list len for further inspections
    """
    log = open(log_file, 'r')
    ids = errors.keys()
    last_line = lsll
    logged = log.readlines()
    flager = len(logged)
    logged = logged[last_line:]
    new_log = open(log.name.replace(".txt", "") + " new.txt", 'a')
    for line in logged:
        last_line += 1
        if line.replace("\n", "") in ids:
            new_log.write(line.replace(line, errors[line.replace("\n", "")])+"\n")
            print line.replace(line, errors[line.replace("\n", "")])+"\n"
        elif len(line) >= 2 and line[-2] in ids:
            new_log.write(line.replace(line[-2], errors[line[-2]]))  # adjust according to protocol
            print (line.replace(line[-2], errors[line[-2]]))
        elif len(re.findall('at[+].*', line.lower())) > 0:
            new_log.write(line.replace("at+", "Working!!!!!"))
            print line.replace(line[0:2], "Working!!!!!")
        else:
            new_log.write(line)
            print line
    new_log.close()
    return last_line, flager
""" 

"""

flag1 = 0
flag = 1
errors_indicator = error_id("errors.xls")  # error DB name addition
name = raw_input("Log name: ")
if name.find(".txt") == -1:
    name += ".txt"
counter = 0
while True:

    time.sleep(0.5)
    if flag1 < flag:
        lsl, flag = log_read(name, errors_indicator)  # log file name addition
        flag1 = flag
        counter = 0
        continue
    elif flag1 == flag and counter < 50:
        counter += 1
        continue
    elif flag == flag1 and counter >= 50:
        break
    else:
        break

